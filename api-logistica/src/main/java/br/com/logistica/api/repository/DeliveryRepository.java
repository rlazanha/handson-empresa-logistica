package br.com.logistica.api.repository;

import br.com.logistica.api.domain.DeliveryDomain;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryRepository extends CrudRepository<DeliveryDomain, Long> {

}
