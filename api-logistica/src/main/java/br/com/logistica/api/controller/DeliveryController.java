package br.com.logistica.api.controller;

import br.com.logistica.api.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/delivery")
public class DeliveryController {

    @Autowired
    private DeliveryService deliveryService;

    @GetMapping
    public ResponseEntity get(){
        return ResponseEntity.ok("OK");
    }

    @GetMapping("/{deliveryId}/step")
    public ResponseEntity getSteps(@PathVariable(value = "deliveryId") String deliveryId) {
        return deliveryService.getStepsByDeliveryId(deliveryId);
    }

}
