package br.com.logistica.api.service.impl;

import br.com.logistica.api.repository.DeliveryRepository;
import br.com.logistica.api.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class DeliveryServiceImpl implements DeliveryService {

    @Autowired
    DeliveryRepository repository;

    @Override
    public ResponseEntity getStepsByDeliveryId(String deliveryId) {
        return ResponseEntity.ok(Arrays.asList(repository.findAll()));
    }
}
