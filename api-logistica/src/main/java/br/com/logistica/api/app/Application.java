package br.com.logistica.api.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "br.com.logistica.api.domain")
@EnableJpaRepositories(basePackages = "br.com.logistica.api.repository")
@ComponentScan(basePackages = "br.com.logistica.api")
public class Application {

    public static void main( String[] args ) {
        SpringApplication.run(Application.class, args);
    }
}
