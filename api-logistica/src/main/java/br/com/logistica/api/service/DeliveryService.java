package br.com.logistica.api.service;

import org.springframework.http.ResponseEntity;

public interface DeliveryService {

    ResponseEntity getStepsByDeliveryId(String deliveryId);
}
